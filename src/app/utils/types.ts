export interface GeneralResponse<T> {
    success: boolean;
    message: string;
    users: T[];
    user: T[];
    numPages: number;
    error: string;
    totalCount: number;
}

export interface User {
    _id: string;
    first_name: string;
    other_name: string;
    paternal_surname: string;
    maternal_surname: string;
    country_work: string;
    identification_type: string;
    identification: string;
    email: string;
    admission_date: Date;
    area: string;
    status: string;
    updated_at: Date;
    createdAt: Date;
    updatedAt: Date;
}