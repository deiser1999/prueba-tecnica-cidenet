import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModules } from './material';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModules,
    ReactiveFormsModule,
    HttpClientModule
  ], 
  exports: [
    MaterialModules,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class SharedModule { }
