import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const usersRoutes: Routes = [
  {
    path: '',
    loadChildren: () => 
      import('./list/list.module').then( m => m.ListModule )
  },
  {
    path: 'user/:id',
    loadChildren: () => 
      import('./user/user.module').then( m => m.UserModule )
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(usersRoutes)
  ]
})
export class UsersModule { }
