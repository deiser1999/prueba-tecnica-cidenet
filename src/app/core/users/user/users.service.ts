import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GeneralResponse, User } from 'src/app/utils/types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }


  public createUser(user: User): Promise<GeneralResponse<User>>{
    return this.http.post<GeneralResponse<User>>(
      `${environment.host}/users/create`, user
    ).toPromise();
  }
}
