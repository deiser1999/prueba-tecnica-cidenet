import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/utils/types';
import { UsersService } from './users.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  // Id del usuario
  public idUser: string = '';
  // Formulario del usuario
  formUser: FormGroup = this.fb.group({});
  // New date
  private readonly date = new Date();
  // Maxima fecha de ingreso
  public maxDate = new Date( this.date.getFullYear(), this.date.getMonth()+1, this.date.getDate()-2 );

  constructor(
    private activateRoute: ActivatedRoute,
    private fb: FormBuilder,
    private _usersService: UsersService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // Desectruramos el parametro id
    const { id } = this.activateRoute.snapshot.params;
    // Guardamos el id en la variable idUser
    this.idUser = id;
    // Iniciamos el formulario
    this.initForm();
  }


  initForm(){
    // Creamos el formulario del usuario
    this.formUser = this.fb.group({
      first_name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(20)
      ])],
      other_name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])],
      paternal_surname: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(20)
      ])],
      maternal_surname: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(20)
      ])],
      country_work: ['', Validators.required],
      identification_type: ['', Validators.required],
      identification: ['', Validators.required],
      email: [{ value: '', disabled: true }],
      admission_date: ['', Validators.required],
      area: ['', Validators.required],
      status: ['Activo', Validators.required]
    });
  } 

  async onSubmit(event: Event) {
    // Evita que se envie el formulario
    event.preventDefault();
    // Detenemos la propagación del evento del formulario
    event.stopPropagation();
    // Validamos el formulario
    if(this.formUser.valid){
      if(this.idUser === 'new_user'){
        // Llama al servicio para crear un usuario
        this.createUser(this.formUser.value);
      }
    }
  }

  async createUser(user: User): Promise<void> {
    // Inicializamos el try catch
    try {
      // Llamamos al servicio
      const response = await this._usersService.createUser(user);
      if(response.success) {
        // Si se creo el usuario
        // Limpiamos el formulario
        this.formUser.reset();
        // Reiniciamos el formulario
        this.initForm();
        // Redirigimos a la lista de usuarios
        this.router.navigate(['/']);
      }
    } catch (error) {
      console.log(error)
    }
  }

}
