import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { User } from 'src/app/utils/types';
import { ListService } from './list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, AfterViewInit {

  // Data source
  public users: User[] = [];
  // Numero de paginas
  public numPages: number = 0;
  // Index de la paginación
  public paginatorIndex: number = 0;
  // Total de usuarios
  public totalCount: number = 0;
  // Nombre de las columnas
  public displayedColumns: string[] = ['identification_type', 'identification', 'names', 'lastnames', 'country_work', 'email', 'status', 'area', 'admission_date'];

  // Paginación de la tabla
  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  constructor(private _listUsersService: ListService) {}

  async ngOnInit(): Promise<void> {
    // Llamamos al método para cargar el datasource
    await this.loadDataSource();
  }

  ngAfterViewInit(): void {
    // Inicializamos el try catch
    try {
      // Si el paginador existe
      if (this.paginator) {
        // Asignamos el evento de cambio de página
        this.paginator.page.subscribe(async (event) => {
          // Llamamos al método para cargar el datasource
          await this.loadDataSource(event.pageIndex + 1);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * @desc Cargar el datasource de la tabla
   */
  public async loadDataSource(poge: number = 1): Promise<void> {
    // Inicializamos el try catch
    try {
      // Obtenemos la respuesta del servicio
      const response = await this._listUsersService.getUsers(poge);
      // Si la respuesta es correcta
      if (response.success) {
        // Asignamos el número de páginas
        this.numPages = response.numPages;
        // Asignamos el arreglo de usuarios
        this.users = response.users;
        // Asignamos el total de usuarios
        this.totalCount = response.totalCount;
      }
    } catch (error) {
      console.log(error);
    }
  }
}
