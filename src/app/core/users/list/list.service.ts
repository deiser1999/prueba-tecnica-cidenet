import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GeneralResponse, User } from 'src/app/utils/types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(
    private http: HttpClient
  ) { }


  /**
   * @description Get all users
   * @author Deyser Andres Orozco Yepes
   * @param {number} page
   * @return {Promise<GeneralResponse>}
   */
  public getUsers( page: number = 1): Promise<GeneralResponse<User>> {
    return this.http.get<GeneralResponse<User>>(
      `${environment.host}users?page=${page}`
    ).toPromise();
  }

}
